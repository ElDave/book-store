namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ccoptional : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCards", "CCNum", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "CCNum", c => c.String(nullable: false));
        }
    }
}
